import Vue from 'vue'
import Vuex from 'vuex'
import gql_queries from '../graphql/queries'
import gql_mutations from '../graphql/mutations'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const state = {
	customers: [],
	admins: [],
	issues: [],
	loadedCustomers : false,
	loadedAdmins : false,
	loadedIssues : false,
	me: null
}

const getters = {
	customers: (state) => {
		return state.customers
	},
	getCustomerById: (state) => (id) => {
		return state.customers.find(c => c.id == id)
	},
	admins: (state) => {
		return state.admins
	},
	getAdminById: (state) => (id) => {
		return state.admins.find(c => c.id == id)
	},
	issues: (state) => {
		return state.issues
	},
	getIssueById: (state) => (id) => {
		return state.issues.find(c => c.id == id)
	},
	me: (state) => {
		return state.me
	},
}

const mutations = {
	INIT_ME (state, me) {
		state.me = me
	},
	INIT_CUSTOMERS (state, customers) {
		state.customers = customers
		state.loadedCustomers = true
	},
	ADD_CUSTOMER (state, customer) {
		state.customers.push(customer)
	},
	UPDATE_CUSTOMER (state, {oldC, newC}) {
		let index = state.customers.indexOf(oldC)
		if(index >= 0) {
			state.customers.splice(index, 1)
			state.customers.splice(index, 0, newC)
		}
	},
	DELETE_CUSTOMER (state, customer) {
		let index = state.customers.indexOf(customer)
		if(index >= 0) state.customers.splice(index, 1)
	},
	INIT_ADMINS (state, admins) {
		state.admins = admins
		state.loadedAdmins = true
	},
	ADD_ADMIN (state, admin) {
		state.admins.push(admin)
	},
	UPDATE_ADMIN (state, {oldC, newC}) {
		let index = state.admins.indexOf(oldC)
		if(index >= 0) {
			state.admins.splice(index, 1)
			state.admins.splice(index, 0, newC)
		}
	},
	DELETE_ADMIN (state, admin) {
		let index = state.admins.indexOf(admin)
		if(index >= 0) state.admins.splice(index, 1)
	},
	INIT_ISSUES (state, issues) {
		state.issues = issues
		state.loadedAdmins = true
	},
	ADD_ISSUE (state, issue) {
		state.issues.push(issue)
	},
	ADD_COMMENT (state, {issue,comment}) {
		let index = state.issues.indexOf(issue)
		if(index >= 0) {
			state.issues[index].comments.push(comment)
		}
	},
	UPDATE_ISSUE (state, {oldC, newC}) {
		let index = state.issues.indexOf(oldC)
		if(index >= 0) {
			state.issues.splice(index, 1)
			state.issues.splice(index, 0, newC)
		}
	},
	DELETE_ISSUE (state, issue) {
		let index = state.issues.indexOf(issue)
		if(index >= 0) state.issues.splice(index, 1)
	},
	DELETE_COMMENT (state, {issue,comment}) {
		let index = state.issues.indexOf(issue)
		if(index >= 0) {
			let ic = state.issues[index].comments.indexOf(comment)
			if(ic >=0) state.issues[index].comments.splice(ic, 1)
		}
	},
}

const actions = {
	loadMe(context) {
		return new Promise((resolve, reject) => {
			apolloClient.query({
		          query: gql_queries.ME
		        })
		        .then(({data}) => {
		          context.commit('INIT_ME',data.me)
		          resolve()
		        })
		        .catch(error => {
		          reject(error)
		        })
		})
	},
	loadCustomers (context) {
		return new Promise((resolve, reject) => {
			apolloClient.query({
		          query: gql_queries.GET_CUSTOMERS
		        })
		        .then(({data}) => {
		          context.commit('INIT_CUSTOMERS',data.customers)
		          resolve()
		        })
		        .catch(error => {
		          reject(error)
		        })
		})
	},
	initCustomers (context, force = false) {
		return new Promise((resolve, reject) => {
			if(!context.state.loadedCustomers || force)
				context.dispatch('loadCustomers')
					.then(() => resolve())
					.catch((error) => reject(error) )
			else resolve()
		})
	},
	createCustomer (context, customer) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.ADD_USER,
		        	variables: {...customer,is_admin: false}
		        })
		        .then(({data}) => {
		        	resolve(data)
					context.commit('ADD_CUSTOMER', data.createOrUpdateUser)
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	updateCustomer (context, customer) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.UPDATE_USER,
		        	variables: customer
		        })
		        .then(({data}) => {
		        	let newC = data.createOrUpdateUser
					let oldC = context.getters.getCustomerById(newC.id)
					resolve(data)
					context.commit('UPDATE_CUSTOMER', {oldC, newC})
		        })
		        .catch(error => {		        	
		        	reject(error)
		        })
		})
	},
	deleteCustomer (context,customer) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.DELETE_USER,
		        	variables: {id: customer.id}
		        })
		        .then(({data}) => {
		        	if(data.deleteUser){
			        	context.commit('DELETE_CUSTOMER', customer)
						resolve(data)
		        	} else reject('Error')
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},

	loadAdmins (context) {
		return new Promise((resolve, reject) => {
			apolloClient.query({
		          query: gql_queries.GET_ADMINS
		        })
		        .then(({data}) => {
		          context.commit('INIT_ADMINS',data.admins)
		          resolve()
		        })
		        .catch(error => {
		          reject(error)
		        })
		})
	},
	initAdmins (context, force = false) {
		return new Promise((resolve, reject) => {
			if(!context.state.loadedAdmins || force)
				context.dispatch('loadAdmins')
					.then(() => resolve())
					.catch((error) => reject(error) )
			else resolve()
		})
	},
	createAdmin (context, admin) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.ADD_USER,
		        	variables: {...admin, is_admin: 1}
		        })
		        .then(({data}) => {
		        	resolve(data)
					context.commit('ADD_ADMIN', data.createOrUpdateUser)
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	updateAdmin (context, admin) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.UPDATE_USER,
		        	variables: admin
		        })
		        .then(({data}) => {
		        	let newC = data.createOrUpdateUser
					let oldC = context.getters.getAdminById(newC.id)
					resolve(data)
					context.commit('UPDATE_ADMIN', {oldC, newC})
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	deleteAdmin (context,admin) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.DELETE_USER,
		        	variables: {id: customer.id}
		        })
		        .then(({data}) => {
		        	if(data.deleteUser){
			        	context.commit('DELETE_ADMIN', admin)
						resolve(data)
					} else rejecct('Error')
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},

	loadIssues (context) {
		return new Promise((resolve, reject) => {
			apolloClient.query({
		          query: gql_queries.GET_ISSUES
		        })
		        .then(({data}) => {
		          context.commit('INIT_ISSUES',data.issues)
		          resolve()
		        })
		        .catch(error => {
		          reject(error)
		        })
		})
	},
	initIssues (context, force = false) {
		return new Promise((resolve, reject) => {
			if(!context.state.loadedIssues || force)
				context.dispatch('loadIssues')
					.then(() => resolve())
					.catch((error) => reject(error) )
			else resolve()
		})
	},
	createIssue (context, issue) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.SUBMIT_ISSUE,
		        	variables: issue
		        })
		        .then(({data}) => {
		        	resolve(data)
					context.commit('ADD_ISSUE', data.submitOrUpdateIssue)
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	addComment (context, {issue,comment}) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.ADD_COMMENT,
		        	variables: comment
		        })
		        .then(({data}) => {
					context.commit('ADD_COMMENT', {issue, comment: data.addOrUpdateComment})
		        	resolve(data)
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	updateIssue (context, issue) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.UPDATE_ISSUE,
		        	variables: issue
		        })
		        .then(({data}) => {
		        	let newC = data.submitOrUpdateIssue
					let oldC = context.getters.getIssueById(newC.id)
					resolve(data)
					context.commit('UPDATE_ISSUE', {oldC, newC})
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	statusIssue (context, {issue,status}) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.STATUS_ISSUE,
		        	variables: {id:issue.id,status}
		        })
		        .then(({data}) => {
		        	let newC = data.changeIssueStatus
					let oldC = context.getters.getIssueById(newC.id)
					resolve(data)
					context.commit('UPDATE_ISSUE', {oldC, newC})
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	deleteIssue (context,issue) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.DELETE_ISSUE,
		        	variables: {id: issue.id}
		        })
		        .then(({data}) => {
		        	if(data.deleteIssue){
			        	context.commit('DELETE_ISSUE', issue)
						resolve(data)
		        	} else reject('Error')
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
	deleteComment (context,{issue,comment}) {
		return new Promise((resolve, reject) => {
			apolloClient.mutate({
		        	mutation: gql_mutations.DELETE_COMMENT,
		        	variables: {id: comment.id}
		        })
		        .then(({data}) => {
		        	if(data.deleteComment){
			        	context.commit('DELETE_COMMENT', {issue,comment})
						resolve(data)
		        	} else reject('Error')
		        })
		        .catch(error => {
		        	reject(error)
		        })
		})
	},
}

export default new Vuex.Store({
	strict: debug,
	state,
	mutations,
	actions,
	getters
})