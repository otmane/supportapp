require('./bootstrap');

//window.Vue = require('vue');
import Vue from 'vue'

import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
import store from './store/store'
import VueRouter from 'vue-router'
import Vuetable from 'vuetable-2'
import VuetablePagination from "vuetable-2/src/components/VuetablePagination"
import VeeValidate from 'vee-validate'

Vue.use(VueRouter)
Vue.use(VueApollo)

const config = {
  errorBagName: 'verrors',
  fieldsBagName: 'vfields'
};

Vue.use(VeeValidate, config);

Vue.component(Vuetable.name, Vuetable);
Vue.component('vuetable-pagination', VuetablePagination);


// HTTP connexion to the API
const httpLink = new HttpLink({
  // You should use an absolute URL here
  uri: 'http://localhost:8000/graphql',
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from localstorage if it exists
  const token = document.head.querySelector('meta[name="jwt-token"]').content;
  // return the headers to the context so httpLink can read them
  return {
      headers: {
          ...headers,
          authorization: token ? `Bearer ${token}` : null,
          accept: 'application/json'
    }
  }
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
window.apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache,
})

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

const routes = [
    {path: '/', name: 'dashboard', component: require('./components/Dashboard.vue').default},
    {path: '/issues', name: 'issues', component: require('./components/Issues.vue').default},
    {path: '/customers', name: 'customers', component: require('./components/Users.vue').default},
    {path: '/admins', name: 'admins', component: require('./components/Users.vue').default},
    
]

const router = new VueRouter({
    routes
})


const app = new Vue({
    el: '#app',
    apolloProvider,
    router,
    store
});
