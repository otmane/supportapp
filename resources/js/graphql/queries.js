import { gql } from "apollo-boost"

const GET_CUSTOMERS = gql`
  query {
    customers {
      id
      name
      email
      created_at
    }
  }
`
const GET_ADMINS = gql`
  query {
    admins {
      id
      name
      email
      created_at
    }
  }
`
const ME = gql`
  query {
    me {
      id
      name
      email
      is_admin
      created_at
    }
  }
`
const GET_ISSUES = gql`
  query {
    issues {
      id
      number
      title
      content
      status
      created_at
      user {
        name
      }
      comments {
        id
        content
        created_at
        user {
          id
          name
        }
      }
    }
  }
`


export default {
  GET_CUSTOMERS,
  GET_ADMINS,
  GET_ISSUES,
  ME
}