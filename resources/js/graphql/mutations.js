import { gql } from "apollo-boost"

const ADD_USER = gql`
    mutation ($name: String!,$email: String!, $password: String!, $password_confirmation: String!, $is_admin: Boolean) {
        createOrUpdateUser(name:$name, email:$email, password:$password, password_confirmation:$password_confirmation, is_admin: $is_admin){
            id
            name
            email
            created_at
        }
    }
`

const UPDATE_USER = gql`
    mutation ($id: Int!, $name: String!,$email: String!) {
        createOrUpdateUser(id: $id, name:$name, email:$email){
            id
            name
            email
            created_at
        }
    }
`
const DELETE_USER = gql`
    mutation ($id: Int!) {
        deleteUser(id: $id)
    }
`



const SUBMIT_ISSUE = gql`
    mutation ($content: String!,$title: String!) {
        submitOrUpdateIssue(content:$content, title:$title){
            id
            number
            title
            content
            status
            created_at
            user {
                name
            }
            comments {
                id
                content
                created_at
                user {
                    id
                    name
                }
            }
        }
    }
`
const UPDATE_ISSUE = gql`
    mutation ($id: Int!,$content: String!,$title: String!) {
        submitOrUpdateIssue(id: $id,content:$content, title:$title){
            id
            number
            title
            content
            status
            created_at
            user {
                name
            }
            comments {
                id
                content
                created_at
                user {
                    id
                    name
                }
            }
        }
    }
` 
const STATUS_ISSUE = gql`
    mutation ($id: Int!,$status: String!) {
        changeIssueStatus(id: $id,status:$status){
            id
            number
            title
            content
            status
            created_at
            user {
                name
            }
            comments {
                id
                content
                created_at
                user {
                    id
                    name
                }
            }
        }
    }
` 
const DELETE_ISSUE = gql`
    mutation ($id: Int!) {
        deleteIssue(id: $id)
    }
`
const ADD_COMMENT = gql`
    mutation ($issue_id: Int!,$content: String!) {
        addOrUpdateComment(issue_id:$issue_id,content:$content){
            id
            issue_id
            content
            created_at
            user {
                id
                name
            }
        }
    }
`
const UPDATE_COMMENT = gql`
    mutation ($id: Int!,$content: String!,$issue_id: Int!) {
        addOrUpdateComment(id: $id,content:$content, issue_id:$issue_id){
            id
            issue_id
            content
            created_at
            user {
                id
                name
            }
        }
    }
`
const DELETE_COMMENT = gql`
    mutation ($id: Int!) {
        deleteComment(id: $id)
    }
`

export default {
    ADD_USER,
    UPDATE_USER,
    DELETE_USER,
    SUBMIT_ISSUE,
    UPDATE_ISSUE,
    STATUS_ISSUE,
    DELETE_ISSUE,
    ADD_COMMENT,
    UPDATE_COMMENT,
    DELETE_COMMENT
}