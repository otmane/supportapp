<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Suppor Issue Information</title>
</head>
<body>
	<p>
		Thank you {{ ucfirst($user->name) }} for contacting our support team. A support issue has been opened for you. You will be notified when a response is made by email. The details of your issue are shown below:
	</p>

	<p>Title: {{ $issue->title }}</p>
	<p>Number: {{ $issue->number }}</p>
	<p>Status: {{ $issue->status }}</p>

</body>
</html>