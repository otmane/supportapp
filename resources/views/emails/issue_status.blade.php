<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Support Issue Status</title>
</head>
<body>
	<p>
		Customer: {{ ucfirst($issueOwner->name) }},
	</p>
	<p>
		The status of the issue Number #{{ $issue->number }} has been changed to "{{ $issue->status }}".
	</p>
</body>
</html>