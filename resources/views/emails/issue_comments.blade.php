<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Suppor Issue</title>
</head>
<body>
	<p>
		{{ $comment->content }}
	</p>

	---
	<p>Replied by: {{ $user->name }}</p>

	<p>Title: {{ $issue->title }}</p>
	<p>Number: {{ $issue->number }}</p>
	<p>Status: {{ $issue->status }}</p>

</body>
</html>