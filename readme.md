## Support web application

### Technology & Tools

Backend :

- Laravel 5.8 (PHP Framework)
- GraphQL (Data query language for API)
- JWT (API authentication access Token)

Frontend :

- VueJS (Javascript Framework)
- VueX (State management pattern)
- Apollo GraphQL (Client engine for communication with GraphQL server)
- Webpack (Module bundler)

### Architecture

The application is designed for full API mode (Sign up, Login...). The client side work in API mode 

Patterns :

- Repository
- State
- ...

### Features & Authorization

Customer Access :

- Registration
- Authentication
- Issues managment (Submit/View/Edit/Delete Issues, Change issue status, Add/Delete Comment)

Admin Access :

- Authentication
- Customers managment (Create/Update/Delete)
- Admins managment (Create/Update/Delete)
- Issues managment (View/Edit/Delete Issues, Change issue status, Add/Delete Comment)

**The Admin cannot submit the issues !**

- Send email automatically after submitting issue, changing her status or adding comment


### Setup

**1** Clone repository
```bash
git clone https://otmane@bitbucket.org/otmane/supportapp.git .
```

**2** Install dependencies via Composer
```bash
composer install
```

**3** Create database `support_db`

**4** Run migrations via artisan
```bash
php artisan migrate
```

**5** Run seeder to create the default admin user :
```bash
php artisan db:seed
```

**6** The default mail driver is `log`, if you want to receive emails configure your SMTP params in `.env` file, and change `MAIL_DRIVER=smtp`

**7** Launch server
```bash
php artisan serve
```
Laravel development server started: `http://127.0.0.1:8000`

**8** Connect to the application with the user `admin@support.com` (password: `azerty`) and start managing the different axes

### JWT Config

- Algo : HS256
- Required claims : iss, iat, nbf, sub, jti
- TTL : never

### WEB

Urls :

- `/` root link
- `/login` 
- `/register` csutomer registration

### API

#### Authentication endpoint:

- /api/auth/login (Login and return JWT Token)
- /api/auth/logout (Logout)
- /api/auth/me (Profil)
- /api/auth/refresh (Refresh token)

#### GraphQL :

GraphQL endpoint : `/graphql`

Types :

- user
- issue
- comment

Queries :

- customers
- admins
- issues
- me

Mutations :

- submitOrUpdateIssue
- deleteIssue
- changeIssueStatus
- deleteComment
- addOrUpdateComment
- createOrUpdateUser
- deleteUser

Methods : GET/POST

#### API usage :

**1** POST to the login endpoint `/api/auth/login` with valid credentials :
```bash
http://localhost:8000/api/auth/login?email=admin@support.com&password=azerty
```
and get a response like : 
```json
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ",
    "token_type": "bearer",
    "expires_in": 0
}
```
This token can then be used to make authenticated requests to your application

**2** Use the GraphQL API with the Authorization and Accept headers
```bash
Authorization: Bearer eyJhbGciOiJIUzI1NiI...
Accept: application/json 
```