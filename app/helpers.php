<?php

if (! function_exists('jwt_token')) {
	function jwt_token()
	{
		return auth()->user() ? auth('api')->tokenById(auth()->user()->id) : null;
	}
}