<?php
namespace App\Services;

use App\Models\Issue;
use Illuminate\Contracts\Mail\Mailer as LaraMailer;
use App\Repositories\UsersRepository;

class Mailer {
	protected $mailer; 
    private $usersRepo;          

	protected $fromAddress = 'support@issues_app.dev';
	protected $fromName = 'Support Issue';
	protected $to;
    protected $subject;
    protected $view;
    protected $data = [];

	public function __construct(LaraMailer $mailer, UsersRepository $usersRepo)
	{
		$this->mailer = $mailer;
		$this->usersRepo = $usersRepo;
	}
	
	/**
	 * Send Issue information to user
	 * 
	 * @param  User   $user
	 * @param  Issue  $issue
	 * @return method deliver()
	 */
	public function sendIssueInformation($user, Issue $issue)
	{
		$this->to = $user->email;
		$this->subject = "[Issue Number: $issue->number] $issue->title";
		$this->view = 'emails.issue_info';
		$this->data = compact('user', 'issue');

		return $this->deliver();
	}

	/**
	 * Send Issue Comments/Replies to Issue Owner
	 *
	 * @param  User   $issueOwner
	 * @param  User   $user
	 * @param  Issue  $issue
	 * @param  Comment  $comment
	 * @return method deliver()
	 */
	public function sendIssueComments($issueOwner, $user, Issue $issue, $comment)
	{
		$this->to = $issueOwner->email;
		$this->subject = "RE: $issue->title (Issue Number: $issue->number)";
		$this->view = 'emails.issue_comments';
		$this->data = compact('issueOwner', 'user', 'issue', 'comment');

		return $this->deliver();
	}

	/**
	 * Send issue status notification
	 * 
	 * @param  User   $issueOwner
	 * @param  Issue  $issue
	 * @return method deliver()
	 */
	public function sendIssueStatusNotification($issueOwner, Issue $issue)
	{
		$admins = $this->usersRepo->admins();
		$emails = [];
		foreach ($admins as $ad) {
			$emails[] = $ad->email;
		}
		$this->to = $emails;
		$this->subject = "RE: $issue->title (Issue Number: $issue->number)";
		$this->view = 'emails.issue_status';
		$this->data = compact('issueOwner', 'issue');

		return $this->deliver();
	}

	/**
	 * Do the actual sending of the mail
	 */
	public function deliver()
	{
		$this->mailer->send($this->view, $this->data, function($message) {
			$message->from($this->fromAddress, $this->fromName)
					->to($this->to)->subject($this->subject);
		});
	}
}