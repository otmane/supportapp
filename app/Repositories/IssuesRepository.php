<?php

namespace App\Repositories;

use App\Models\Issue;
use App\Models\Comment;
use App\Services\Mailer;

class IssuesRepository {

	private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

	public function list($user_id = null)
	{	
		return $user_id ? Issue::where('user_id',$user_id)->get() : Issue::all();
	}

	public function getById($id)
	{	
		return Issue::findOrFail($id);
	}

	public function getCommentById($id)
	{	
		return Comment::findOrFail($id);
	}

	public function submitOrUpdate($attr)
	{
		$issue = Issue::updateOrCreate(
            ['id' => $attr['id'] ?? null],
            ['title' => $attr['title'],'content' => $attr['content'],'user_id' => auth()->user()->id]
        );

		if(!isset($attr['id']) || !$attr['id']){
        	// send email to customer
			$this->mailer->sendIssueInformation(auth()->user(),$issue);
		}

        return $issue;
	}

	public function delete($id)
	{
		$issue = $this->getById($id);
		return $issue->delete();
	}

	public function changeStatus($id, $status)
	{
		$issue = $this->getById($id);
		$issue->update(['status' => $status]);

		// send email to all admins
		$this->mailer->sendIssueStatusNotification(auth()->user(),$issue);
		return $issue;
	}

	public function submitOrUpdateComment($attr)
	{
		$comment = Comment::updateOrCreate(
            ['id' => $attr['id'] ?? null],
            ['issue_id' => $attr['issue_id'],'content' => $attr['content'],'user_id' => auth()->user()->id]
        );

        $issue = $comment->issue;

        // send email to customer
		$this->mailer->sendIssueComments($issue->user, auth()->user(), $issue, $comment);
		
		return $comment;
	}

	public function deleteComment($id)
	{
		$comment = $this->getCommentById($id);
		return $comment->delete();
	}
    
}