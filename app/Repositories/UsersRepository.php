<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersRepository {

	public function admins()
	{	
		return User::where('is_admin',true)->get();
	}

	public function customers()
	{	
		return User::where('is_admin',false)->get();
	}

	public function getById($id)
	{	
		return User::findOrFail($id);
	}

	public function createOrUpdate($attr)
	{
		if(isset($attr['id']) && $attr['id']){
			$user = $this->getById($attr['id']);
			$user->update([
            	'name' => $attr['name'],
            	'email' => $attr['email']
            ]);
		} else {
			$user = User::create([
            	'name' => $attr['name'],
            	'email' => $attr['email'],
            	'password' => Hash::make($attr['password']),
            	'is_admin' => $attr['is_admin']
            ]);
		}

        return $user;
	}

	public function delete($id)
	{
		$user = $this->getById($id);
		return $user->delete();
	}
    
}