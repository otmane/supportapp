<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
	 * The attributes that are mass assignable.
	 * 
	 * @var  array
	 */
	protected $fillable = [
		'issue_id', 'user_id', 'content'
	];
    /**
     * A comment belongs to a particular issue
     */
    public function issue()
    {
    	return $this->belongsTo(Issue::class);
    }
    /**
     * A comment belongs to a user
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
