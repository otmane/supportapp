<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'number', 'title','content','status'
    ];

    /**
     * An issue belongs to a user
     */
	public function user()
    {
    	return $this->belongsTo(User::class);
   }
    /**
     * An issue can have many comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public static function boot()
    {
        parent::boot();    

        static::creating(function($issue)
        {
            
            $issue->number = strtoupper(str_random(10));
            $issue->status = 'Submitted';
        });
    }
}
