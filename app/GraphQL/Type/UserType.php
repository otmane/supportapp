<?php

namespace App\GraphQL\Type;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Rebing\GraphQL\Support\Facades\GraphQL;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'A user',
        'model' => User::class,
    ];

    public function fields()
    {
        return [
          'id' => [
            'type' => Type::nonNull(Type::int()),
            'description' => 'The id of a user'
          ],
          'name' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The name of a user'
          ],
          'email' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The email address of a user'
          ],
          'is_admin' => [
            'type' => Type::boolean(),
            'description' => 'True, if the user is an admin'
          ],
          'issues' => [
          	'type' => Type::listOf(GraphQL::type('issue')),
          	'description' => 'The user issues'
          ],
          'created_at' => [
            'type' => Type::string(),
            'description' => 'Date was created'
          ],
          'updated_at' => [
            'type' => Type::string(),
            'description' => 'Date was updated'
          ],
        ];
    }
}