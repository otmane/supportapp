<?php

namespace App\GraphQL\Type;

use App\Models\User;
use App\Models\Issue;
use App\Models\Comment;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CommentType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Comment',
        'description' => 'A comment',
        'model' => Comment::class
    ];

    public function fields()
    {
        return [
          'id' => [
            'type' => Type::nonNull(Type::int()),
            'description' => 'The id of a comment'
          ],
          'content' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The content of a comment'
          ],
          'user' => [
            'type' => GraphQL::type('user'),
            'description' => 'The comment user'
          ],
          'issue_id' => [
            'type' => Type::nonNull(Type::int()),
            'description' => 'The comment issue id'
          ],
          'issue' => [
            'type' => GraphQL::type('issue'),
            'description' => 'The comment issue'
          ],
          'created_at' => [
            'type' => Type::string(),
            'description' => 'Date was created'
          ],
          'updated_at' => [
            'type' => Type::string(),
            'description' => 'Date was updated'
          ],
        ];
    }
}