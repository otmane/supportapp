<?php

namespace App\GraphQL\Type;

use App\Models\User;
use App\Models\Issue;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Rebing\GraphQL\Support\Facades\GraphQL;

class IssueType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Issue',
        'description' => 'An issue',
        'model' => Issue::class,
    ];

    public function fields()
    {
        return [
          'id' => [
            'type' => Type::nonNull(Type::int()),
            'description' => 'The id of an issue'
          ],
          'number' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The number of an issue'
          ],
          'title' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The title of an issue'
          ],
          'content' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The content of an issue'
          ],
          'status' => [
            'type' => Type::nonNull(Type::string()),
            'description' => 'The status of an issue'
          ],
          'user' => [
          	'type' => GraphQL::type('user'),
          	'description' => 'The issue user'
          ],
          'comments' => [
          	'type' => Type::listOf(GraphQL::type('comment')),
          	'description' => 'The issue comments'
          ],
          'created_at' => [
            'type' => Type::string(),
            'description' => 'Date was created'
          ],
          'updated_at' => [
            'type' => Type::string(),
            'description' => 'Date was updated'
          ],
        ];
    }
}