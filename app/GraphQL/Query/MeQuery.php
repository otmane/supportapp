<?php

namespace App\GraphQL\Query;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;

class MeQuery extends Query
{
    protected $attributes = [
        'name' => 'MeQuery',
        'description' => 'A query'
    ];

    public function type()
    {
        return GraphQL::type('user');
    }

    public function args()
    {
        return [

        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return auth()->user();
    }
}