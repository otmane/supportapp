<?php

namespace App\GraphQL\Query;

use App\Models\Issue;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;
use App\Repositories\IssuesRepository;

class IssuesQuery extends Query
{
    protected $attributes = [
        'name' => 'IssuesQuery',
        'description' => 'A query'
    ];

    private $issusRepo;

    public function __construct(IssuesRepository $issusRepo)
    {
        $this->issusRepo = $issusRepo;
    }

    public function type()
    {
        return Type::listOf(GraphQL::type('issue'));
    }

    public function args()
    {
        return [
            
        ];
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return auth()->user()->is_admin ? $this->issusRepo->list() : $this->issusRepo->list(auth()->user()->id);
    }
}