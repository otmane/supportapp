<?php

namespace App\GraphQL\Query;

use App\Models\User;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Query;
use App\Repositories\UsersRepository;

class AdminsQuery extends Query
{
    protected $attributes = [
        'name' => 'AdminsQuery',
        'description' => 'A query'
    ];

    private $usersRepo;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function type()
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args()
    {
        return [

        ];
    }

    public function authorize($args)
    {
        return auth()->user()->is_admin;
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->usersRepo->admins();
    }
}