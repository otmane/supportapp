<?php

namespace App\GraphQL\Mutation;

use App\Models\Issue;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\IssuesRepository;

class SubmitOrUpdateIssueMutation extends Mutation
{
    protected $attributes = [
        'name' => 'SubmitOrUpdateIssue',
        'description' => 'Submit a new issue or update an existing one'
    ];

    private $issusRepo;

    public function __construct(IssuesRepository $issusRepo)
    {
        $this->issusRepo = $issusRepo;
    }

    public function type()
    {
        return GraphQL::type('issue');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'title' => ['name' => 'title', 'type' => Type::nonNull(Type::string()), 'rule' => ['required','string','min:2']],
            'content' => ['name' => 'content', 'type' => Type::nonNull(Type::string()), 'rule' => ['required','string','min:2']],
        ];
    }

    public function authorize($args)
    {
        /* The customer can submit a new issue
         * the customer can update only his issues
         * the admin cannot create the issues */
        
        if(isset($args['id']) && $args['id']) { // update mode
            return auth()->user()->is_admin ? true : auth()->user()->id == Issue::find($args['id'])->user_id;
        } else { // create mode
            return !auth()->user()->is_admin;
        }
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->issusRepo->submitOrUpdate($args);
    }
}