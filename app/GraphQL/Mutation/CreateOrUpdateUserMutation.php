<?php

namespace App\GraphQL\Mutation;

use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\UsersRepository;

class CreateOrUpdateUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateOrUpdateUser',
        'description' => 'A mutation'
    ];

    private $usersRepo;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function type()
    {
        return GraphQL::type('user');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string()), 'rule' => ['required','string','min:2']],
            'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string()), 'rule' => ['required','email','unique:users']],
            'password' => ['name' => 'password', 'type' => Type::string(), 'rule' => ['required_without:id','confirmed']],
            'password_confirmation' => ['name' => 'password_confirmation', 'type' => Type::string(), 'rule' => ['required_with:password']],
            'is_admin' => ['name' => 'is_admin', 'type' => Type::boolean()],
        ];
    }

    public function validationErrorMessages ($args = []) 
    {
        return [
            'email.unique' => 'Sorry, this email address is already in use',                     
        ];
    }

    public function authorize($args)
    {
        return auth()->user()->is_admin;
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->usersRepo->createOrUpdate($args);
    }
}