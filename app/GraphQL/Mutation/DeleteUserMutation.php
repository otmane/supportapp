<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\UsersRepository;

class DeleteUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'DeleteUser',
        'description' => 'A mutation'
    ];

    private $usersRepo;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function type()
    {
        return Type::boolean();
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int()),'rules' => ['required', 'exists:users,id']],
        ];
    }

    public function authorize($args)
    {
        return auth()->user()->is_admin;
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->usersRepo->delete($args['id']);
    }
}