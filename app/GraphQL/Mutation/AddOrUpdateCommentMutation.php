<?php

namespace App\GraphQL\Mutation;

use App\Models\Issue;
use App\Models\Comment;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\IssuesRepository;

class AddOrUpdateCommentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'AddOrUpdateCommentMutation',
        'description' => 'A mutation'
    ];

    private $issusRepo;

    public function __construct(IssuesRepository $issusRepo)
    {
        $this->issusRepo = $issusRepo;
    }

    public function type()
    {
        return GraphQL::type('comment');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'issue_id' => ['name' => 'issue_id', 'type' => Type::nonNull(Type::int()),'rules' => ['required', 'exists:issues,id']],
            'content' => ['name' => 'content', 'type' => Type::nonNull(Type::string()),'rules' => ['required', 'string','min:1']],
        ];
    }

    public function authorize($args)
    {
        // The customer can add/update comment only for his issues
        
        if(isset($args['id']) && $args['id']) { // update mode
            return auth()->user()->id == Comment::find($args['id'])->user_id;
        } else { // create mode
            return auth()->user()->is_admin ? true : auth()->user()->id == Issue::find($args['issue_id'])->user_id;
        }
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->issusRepo->submitOrUpdateComment($args);
    }
}