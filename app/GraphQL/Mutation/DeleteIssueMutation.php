<?php

namespace App\GraphQL\Mutation;

use App\Models\Issue;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\IssuesRepository;

class DeleteIssueMutation extends Mutation
{
    protected $attributes = [
        'name' => 'DeleteIssue',
        'description' => 'Delete issue'
    ];

    private $issusRepo;

    public function __construct(IssuesRepository $issusRepo)
    {
        $this->issusRepo = $issusRepo;
    }

    public function type()
    {
        return Type::boolean();
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int()),'rules' => ['required', 'exists:issues,id']],
        ];
    }

    public function authorize($args)
    {
        /* The customer can delete only his issues
         * the admin can delete all issues */

        return auth()->user()->is_admin ? true : auth()->user()->id == Issue::find($args['id'])->user_id;
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->issusRepo->delete($args['id']);
    }
}