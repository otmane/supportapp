<?php

namespace App\GraphQL\Mutation;

use App\Models\Comment;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\IssuesRepository;

class DeleteCommentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'DeleteComment',
        'description' => 'Delete comment'
    ];

    private $issusRepo;

    public function __construct(IssuesRepository $issusRepo)
    {
        $this->issusRepo = $issusRepo;
    }

    public function type()
    {
        return Type::boolean();
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int()),'rules' => ['required', 'exists:comments,id']],
        ];
    }

    public function authorize($args)
    {
        /* The customer/admin can delete only his comment */

        return auth()->user()->id == Comment::find($args['id'])->user_id;
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->issusRepo->deleteComment($args['id']);
    }
}