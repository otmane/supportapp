<?php

namespace App\GraphQL\Mutation;

use App\Models\Issue;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use App\Repositories\IssuesRepository;

class ChangeIssueStatusMutation extends Mutation
{
    protected $attributes = [
        'name' => 'ChangeIssueStatus',
        'description' => 'Change issue status'
    ];

    private $issusRepo;

    public function __construct(IssuesRepository $issusRepo)
    {
        $this->issusRepo = $issusRepo;
    }

    public function type()
    {
        return GraphQL::type('issue');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int(),'rules' => ['required', 'exists:issues,id']],
            'status' => ['name' => 'status', 'type' => Type::nonNull(Type::string()),'rules' => ['required', 'in:In Progress,Resolved,Closed']],
        ];
    }

    public function authorize($args)
    {
        /* The customer can change status only for his issues (Closed)
         * the admin can change status for all issues */

        return auth()->user()->is_admin ? true : (auth()->user()->id == Issue::find($args['id'])->user_id && $args['status'] == "Closed");
    }

    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        return $this->issusRepo->changeStatus($args['id'],$args['status']);
    }
}