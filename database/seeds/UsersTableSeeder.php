<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// check if the user already exist
    	$user = User::where('email','admin@support.com')->first();
    	if(!$user){
    		$user = User::create([
	        	'email' => 'admin@support.com',
	        	'name' => 'Admin',
	        	'password' => \Hash::make('azerty'),
	        	'is_admin' => true
	        ]);
    	}
        
    }
}
